To run Baseline.py file which implements Logistic Regression and LinearSVC on the labelled data
    1. Copy Baseline.py, train.txt and test.txt into the same folder
    2. Run Baseline.py file using python command


To run labelling.py file which contains some rules to label the 30M dataset

    1. copy labelling.py file to a new folder
    2. download fqFiltered.txt and fqFiltered2R.txt files for the 30M dataset and move to the same folder
    3. Run labelling.py file using python command

    The output should be fqFiltered.csv and fqFiltered2R.csv files which are the original dataset saved as csv format after cleaning some unwanted tags in them

    The other files will be loc.csv, enty.csv, num.csv, hum.csv, desc.csv are the files containing questions of the same label and an unlabelled.csv file containing unlabelled questions.

    Also if you re-run the code please delete any csv files that may be present in the folder.


Note : labelling.py execution may take some time to complete as it looks over 30M dataset to label them. The code labels the questions in chunks of 10M each to prevent the memory issue.


To run augment.py file which adds number of labelled quesitons to the train data and computes the accuracy
    1.copy augment.py in the same folder as the output files of labelling.py exists (i.e the folder should contain loc.csv, enty.csv, num.csv, hum.csv, desc.csv that are output by executing the labelling.py) and the normal train.txt and test.txt files.
    2. Run augment.py file using python command    

Note : While adding additional data to the training the data for label "NUM" will be added to a maximum of 296 and "ABBR" is none.


The number of entries for each label in the Test data 

    ABBR    - 9
    DESC    - 138
    ENTY    - 94
    HUM     - 65
    LOC     - 81
    NUM     - 113
   --------------- 
    Total   - 500
