import numpy as np
import csv
import pandas as pd
import os
import json
from spacy.en import English
from keras.utils import np_utils
from collections import OrderedDict
from keras.preprocessing import sequence
from keras.models import Sequential
from keras.layers import Dense, Dropout, Activation, Embedding
from keras.layers import LSTM, SimpleRNN, GRU

#building dictionary
def build_dictionary(data_to_dict,file_to_write):
        word_freqs = OrderedDict()
        for line in data_to_dict:
            words_in = line.strip().split(' ')
            for w in words_in:
                if w not in word_freqs:
                    word_freqs[w] = 0
                word_freqs[w] += 1
        words = word_freqs.keys()
        freqs = word_freqs.values()

        sorted_idx = np.argsort(freqs)
        sorted_words = [words[ii] for ii in sorted_idx[::-1]]

        worddict = OrderedDict()
        worddict['eos'] = 0
        worddict['UNK'] = 1
        for ii, ww in enumerate(sorted_words):
            worddict[ww] = ii+2

        with open('%s'%file_to_write, 'wb') as f:
            json.dump(worddict, f, indent=2, ensure_ascii=False)

        print 'Done'

def lemmatize(data):
    '''
    Given an Natural language text as input in a list it returns the  lemmatized test
    '''
    lemmatizedData = []
    for ques in data:
        ques = nlp(unicode(ques, "utf-8"))
        lemmatizedData.append(" ".join(token.lemma_ for token in ques))
    #Remove unicode in the sentence
    lemmatizedData = [x.encode('utf-8') for x in lemmatizedData]
    return lemmatizedData

# for POS tags
def tagger(data):
    '''
    Given an Natural language text as input in a list it returns the  part of speech tag
    '''
    pos_tags = []
    for ques in data:
        ques = nlp(unicode(ques, "utf-8"))
        pos_tags.append(" ".join(t.tag_ for t in ques))
    #Remove unicode in the sentence
    pos_tags = [x.encode('utf-8') for x in pos_tags]
    return pos_tags

if __name__ == '__main__':
    trainData = pd.read_csv("train.csv", header=0, delimiter="\t", quoting=3)
    testData = pd.read_csv("test.csv", header=0, delimiter="\t", quoting=3)

    embeddings_index = {}
    f = open('/home/vevake/Downloads/glove.6B/glove.6B.100d.txt')
    for emb in f:
        values = emb.split()
        word = values[0]
        coefs = np.asarray(values[1:], dtype='float32')
        embeddings_index[word] = coefs
    f.close()

    #build dictionary
    nlp = English(parser=False)
    train_lemma = lemmatize(trainData['question'])
    train_pos = tagger(trainData['question'])
    trainData['question'] = trainData['question'] + " " + train_lemma + " " + train_pos
    # trainData['question'] = pd.DataFrame(train_lemma)+ " " + pd.DataFrame(train_pos)
    # trainData['question'] = pd.DataFrame(train_pos)
    build_dictionary(trainData['question'],'vocab.json')

    test_lemma = lemmatize(testData['question'])
    test_pos = tagger(testData['question'])
    testData['question'] = testData['question'] + " " + test_lemma + " " + test_pos
    # testData['question'] = pd.DataFrame(test_lemma)+ " " + pd.DataFrame(test_pos)
    # testData['question'] = pd.DataFrame(test_pos)

    vocab = json.loads(open('vocab.json').read())

    x_train = []
    for ques in trainData['question']:
        s = ques.strip().split()
        x_train.append([vocab[x] if x in vocab else 1 for x in s])

    class_ = {'ABBR': 0, 'DESC': 1, 'ENTY': 2, 'HUM': 3, 'LOC' : 4, 'NUM': 5 }
    y_train = []
    for label in trainData['label']:
        y_train.append([class_[label]])

    x_test = []
    for ques in testData['question']:
        s = ques.strip().split()
        x_test.append([vocab[x] if x in vocab else 1 for x in s])

    y_test = []
    for label in testData['label']:
        y_test.append([class_[label]])


    x_train = sequence.pad_sequences(x_train, maxlen=80)
    y_train = np_utils.to_categorical(y_train, nb_classes=6)

    x_test = sequence.pad_sequences(x_test, maxlen=80)
    y_test = np_utils.to_categorical(y_test, nb_classes=6)

    vocab_dim = 100 # dimensionality of your word vectors
    vocab_len = len(vocab)
    embedding_weights = np.zeros((vocab_len,vocab_dim))
    for word,index in vocab.items():
        embedding_vector = embeddings_index.get(word)
        if embedding_vector is not None :
            embedding_weights[index,:] = embedding_vector
            

    model = Sequential()
    model.add(Embedding(output_dim=100, input_dim=vocab_len, weights=[embedding_weights])) 
    model.add(LSTM(100, return_sequences=False))  
    model.add(Dense(6, activation='softmax'))
    model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])
    model.summary()
    model.fit(x_train, y_train, nb_epoch=20, batch_size=128)

    score,acc = model.evaluate(x_test,y_test)
    print score, acc
