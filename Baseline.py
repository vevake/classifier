#locally disables line-too-long # pylint: disable=C0301
#locally diables invalid-name # pylint: disable=C0103
'''
This program is a baseline classifier using Logistic and LinearSVC model for classifying questions into 6 coarse classes
'''

import csv
import pandas as pd
from spacy.en import English
from sklearn import svm
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
# from sklearn.grid_search import GridSearchCV
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score


#training and test file locations
TRAIN_FILE = "train.txt"
TEST_FILE = "test.txt"
#------------------------------------------------------------------------------------------------------------

def parse_data(file_to_parse, file_to_save):
    '''
    Method to parse input data into a csv file of format <id> , <question> , <label>
    '''

    #parse training and test data and save in csv file
    with open(file_to_parse, "r") as file1, open(file_to_save, "w") as file2:
        writer = csv.writer(file2, delimiter='\t', lineterminator='\n')
        writer.writerow(['id', 'question', 'label'])
        i = 0
        for line in file1:
            line = line.strip()
            label, question = line.split(' ', 1)
            #print(question,label.split(':')[0])
            writer.writerow([i, question, label.split(':')[0]])
            i += 1


def lemmatize(data):
    '''
    Given an Natural language text as input in a list it returns the  lemmatized test
    '''
    lemmatizedData = []
    for ques in data:
        ques = nlp(unicode(ques, "utf-8"))
        lemmatizedData.append(" ".join(token.lemma_ for token in ques))
    #Remove unicode in the sentence
    lemmatizedData = [x.encode('utf-8') for x in lemmatizedData]
    return lemmatizedData

if __name__ == '__main__':
    #parse train data and save as train.csv
    print "Parsing the train data....."
    parse_data(TRAIN_FILE, "train.csv")

    #parse test data and save as csv
    print "Parsing the test data....."
    parse_data(TEST_FILE, "test.csv")

    trainData = pd.read_csv("train.csv", header=0, delimiter="\t", quoting=3)
    testData = pd.read_csv("test.csv", header=0, delimiter="\t", quoting=3)

    #lemmatizing words
    print "Lemmatizing the questions....."
    nlp = English()
    trainDataLemma = lemmatize(trainData["question"])
    testDataLemma = lemmatize(testData["question"])

    training = trainData["question"] + " "+ trainDataLemma
    testing = testData["question"] + " " + testDataLemma

    #fitting CountVectorizer on training question
    print "Converting data to matrix of token counts using CountVectorizer....."
    vectorizer = CountVectorizer(analyzer="word", tokenizer=None, preprocessor=None, stop_words=None, ngram_range=(1, 2), max_features=30000)
    trainDataFeatures = vectorizer.fit_transform(training)

    #fitting CountVectorizer on testing question
    testDataFeatures = vectorizer.transform(testing)

    # Logistic Regression
    logreg = LogisticRegression()
    logreg.fit(trainDataFeatures, trainData["label"])
    resultLogreg = logreg.predict(testDataFeatures)
    outputLogreg = pd.DataFrame(data={"id":testData["id"], "question":testData["question"], "original_label":testData["label"], "output_label":resultLogreg})
    print "Results for Logistic Regression"
    print "F1_score = {}".format(f1_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    print "Precision = {}".format(precision_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    print "Recall = {}".format(recall_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    print "Accuracy = {}".format(accuracy_score(outputLogreg["original_label"], outputLogreg["output_label"]))

    # LinearSVC
    clf = svm.LinearSVC()
    clf.fit(trainDataFeatures, trainData["label"])
    resultLinearSVC = clf.predict(testDataFeatures)
    outputLinearSVC = pd.DataFrame(data={"id":testData["id"], "question":testData["question"], "original_label":testData["label"], "output_label":resultLinearSVC})
    print "Results for LinearSVC"
    print "F1_score = {}".format(f1_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    print "Precision = {}".format(precision_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    print "Recall = {}".format(recall_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    print "Accuracy = {}".format(accuracy_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"]))

    # #Grid Search for logistic Regression
    # print "Performing GridSearch for Logistic Regression....."
    # logreg = LogisticRegression()
    # param = dict(C=[0.001, 0.01, 0.1, 1], penalty=['l1', 'l2'], class_weight=[None, 'balanced'])
    # grid_search = GridSearchCV(logreg, param_grid=param, cv=10)
    # grid_search.fit(trainDataFeatures, trainData["label"])
    # print grid_search.best_estimator_
    # resultLogreg = grid_search.predict(testDataFeatures)
    # outputLogreg = pd.DataFrame(data={"id":testData["id"], "question":testData["question"], "original_label":testData["label"], "output_label":resultLogreg})

    # print "Results for Logistic Regression"
    # print "F1_score = {}".format(f1_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    # print "Precision = {}".format(precision_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    # print "Recall = {}".format(recall_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    # print "Accuracy = {}".format(accuracy_score(outputLogreg["original_label"], outputLogreg["output_label"]))


    # #Grid Search for LinearSVC
    # print "Performing GridSearch for LinearSVC....."
    # clf = svm.LinearSVC()
    # param = dict(C=[0.001, 0.01, 0.1, 1], penalty=['l2'], class_weight=[None, 'balanced'], multi_class=['ovr', 'crammer_singer'])
    # grid_search = GridSearchCV(clf, param_grid=param, cv=10)
    # grid_search.fit(trainDataFeatures, trainData["label"])
    # print grid_search.best_estimator_
    # resultLinearSVC = grid_search.predict(testDataFeatures)
    # outputLinearSVC = pd.DataFrame(data={"id":testData["id"], "question":testData["question"], "original_label":testData["label"], "output_label":resultLinearSVC})

    # print "Results for LinearSVC"
    # print "F1_score = {}".format(f1_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    # print "Precision = {}".format(precision_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    # print "Recall = {}".format(recall_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    # print "Accuracy = {}".format(accuracy_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"]))
