#locally disables line-too-long # pylint: disable=C0301
#locally diables invalid-name # pylint: disable=C0103
'''
This file contains code that parses the 30M dataset and labels them based on some rules.
'''

#import libraries
import csv
import pandas as pd

#30M dataset corpus
FILE_1 = "fqFiltered.txt"
FILE_2 = "fqFiltered2R.txt"

#------------------------------------------------------------------------------------------------

#parse data and  save in csv file
#fqFiltered2R and fqFiltered corresponds to the questions file output by Neural network
def parse_data(file_to_parse):
    '''
    method used to parse the dataset into files of each 1M questions with header
    '''
    file1 = open(file_to_parse, "r")
    f = open(file_to_parse.strip('.txt') + ".csv", "w")
    output = csv.writer(f, delimiter='\t', lineterminator='\n')
    output.writerow(['id', 'subject', 'tags', 'object', 'question'])
    i = 0
    for line in file1:
        line = line.strip()
        subject, relationship, obj, question = line.split('\t', 3)
        relationship = relationship.split('/')
        if "www.freebase.com" in relationship:
            relationship.remove("www.freebase.com")
        elif "rdf.freebase.com" in relationship:
            relationship = [e.replace(">", "").split('.') for e in relationship if e not in ('<http:', '', 'rdf.freebase.com', 'ns')][0]
        output.writerow([i, subject, relationship, obj, question])
    f.close()


def label_data(data):
    '''
    Contains rules to label questions
    returns labelled questions as pandas dataframe
    '''
    #Some preliminary rules to label the questions
    # if the question starts with "who|whom" the answer label will be HUMAN
    data.loc[data.question.str.startswith('who '), 'label'] = 'HUM'
    data.loc[data.question.str.startswith('whom '), 'label'] = 'HUM'
    data.loc[(data['label'] == '') & (data.tags.map(lambda x: 'players' in x)), 'label'] = 'HUM'

    # if the question starts with "where" the answer label will be LOCATION
    data.loc[data.question.str.startswith('where '), 'label'] = 'LOC'
    data.loc[(data.tags.map(lambda x: all(s in 'music artist origin' for s in x))), 'label'] = 'LOC'
    data.loc[(data.tags.map(lambda x: 'country' in x)), 'label'] = 'LOC'
    data.loc[(data.tags.map(lambda x: all(s in 'people person nationality' for s in x))), 'label'] = 'LOC'
    data.loc[(data.tags.map(lambda x: all(s in 'people person place_of_birth' for s in x))), 'label'] = 'LOC'
    data.loc[(data.tags.map(lambda x: all(s in 'location location containedby' for s in x))), 'label'] = 'LOC'
    data.loc[(data.tags.map(lambda x: all(s in 'location location contains' for s in x))), 'label'] = 'LOC'


    # if the question starts with "how many|much|long" the answer label will be NUMERICAL
    data.loc[data.question.str.startswith('how many'), 'label'] = 'NUM'
    data.loc[data.question.str.startswith('how much'), 'label'] = 'NUM'
    data.loc[data.question.str.startswith('how long'), 'label'] = 'NUM'

    # if the question starts with "how" but not assigned the label "NUM" by earlier rules then the answer label will be DESCRIPTION
    data.loc[(data['label'] == '') & (data.question.str.startswith('how ')), 'label'] = 'DESC'
    data.loc[(data.question.str.contains('about ?')) & data.tags.map(lambda x: any(s in 'subjects' for s in x)), 'label'] = 'DESC'

    #the question with tags genre should have the answer label ENTITY because all those answers relate to a distinct thing
    data.loc[(data.tags.map(lambda x: 'genre' in x)), 'label'] = 'ENTY'
    data.loc[(data.tags.map(lambda x: all(s in 'people person gender' for s in x))), 'label'] = 'ENTY'
    data.loc[(data.tags.map(lambda x: all(s in 'people person profession' for s in x))), 'label'] = 'ENTY'
    data.loc[(data.tags.map(lambda x: all(s in 'people person ethnicity' for s in x))), 'label'] = 'ENTY'
    data.loc[(data.tags.map(lambda x: all(s in 'people person religion' for s in x))), 'label'] = 'ENTY'
    data.loc[(data.tags.map(lambda x: all(s in 'people person languages' for s in x))), 'label'] = 'ENTY'

    return data


def save_to_files(data):
    '''
    Saves the lebelled data
    '''
    #writing_to_file
    # data = data.drop(['tags'], axis=1)
    f = open('loc.csv', 'a')
    data[data['label'] == 'LOC'].to_csv(f, header=False, index=False, sep='\t', encoding='utf-8')
    f.close()
    f = open('hum.csv', 'a')
    data[data['label'] == 'HUM'].to_csv(f, header=False, index=False, sep='\t', encoding='utf-8')
    f.close()
    f = open('desc.csv', 'a')
    data[data['label'] == 'DESC'].to_csv(f, header=False, index=False, sep='\t', encoding='utf-8')
    f.close()
    f = open('enty.csv', 'a')
    data[data['label'] == 'ENTY'].to_csv(f, header=False, index=False, sep='\t', encoding='utf-8')
    f.close()
    f = open('num.csv', 'a')
    data[data['label'] == 'NUM'].to_csv(f, header=False, index=False, sep='\t', encoding='utf-8')
    f.close()
    f = open('unlabelled.csv', 'a')
    data[data['label'] == ''].to_csv(f, header=False, index=False, sep='\t', encoding='utf-8')
    f.close()
    # data[data['label'] == 'ABBR'].to_csv('abbr.csv', index=False, sep='\t', encoding='utf-8')


if __name__ == '__main__':
    # invoking parse_data methode to split and preprocess the questions dataset
    # print "Parsing the file_1....."
    # parse_data(FILE_1)
    # print "Parsing the file_2....."
    # parse_data(FILE_2)

    files_to_label = [FILE_1.strip('.txt') + ".csv", FILE_2.strip('.txt') + ".csv"]
    #labelling data and saving it in appropriate files
    for filename in files_to_label:
        print "Processing file :", filename, "as chunks of 10M questions each"
        chunksize = 10 ** 7 #process 10M at a time
        chunks = pd.read_csv(filename, chunksize=chunksize, header=0, delimiter="\t")
        for i, data in enumerate(chunks):
            print "Chunk ", i
            data = data.drop(['subject', 'object'], axis=1)
            data['label'] = ''
            print "labelling"
            labelled_data = label_data(data)
            print "saving"
            save_to_files(labelled_data)
