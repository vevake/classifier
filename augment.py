#locally disables line-too-long # pylint: disable=C0301
#locally diables invalid-name # pylint: disable=C0103

'''
This file augments a  number of data to the original training set and computes the accuracy
'''

#import libraries
import csv
import pandas as pd
import numpy as np
from spacy.en import English
from sklearn import svm
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score, f1_score, precision_score, recall_score
# from sklearn.metrics import confusion_matrix

#number of questions to augment
NUMBER_TO_ADD = 50
TRAIN_FILE = "train.txt"
TEST_FILE = "test.txt"

#---------------------------------------------------------------------------------------------------

def parse_data(file_to_parse, file_to_save):
    '''
    Method to parse input data into a csv file of format <id> , <question> , <label>
    '''

    #parse training and test data and save in csv file
    with open(file_to_parse, "r") as file1, open(file_to_save, "w") as file2:
        writer = csv.writer(file2, delimiter='\t', lineterminator='\n')
        writer.writerow(['id', 'question', 'label'])
        i = 0
        for line in file1:
            line = line.strip()
            label, question = line.split(' ', 1)
            #print(question,label.split(':')[0])
            writer.writerow([i, question, label.split(':')[0]])
            i += 1

def lemmatize(data):
    '''
    Given an Natural language text as input in a list it returns the  lemmatized test
    '''
    lemmatizedData = []
    for ques in data:
        ques = nlp(unicode(ques, "utf-8"))
        lemmatizedData.append(" ".join(token.lemma_ for token in ques))
    #Remove unicode in the sentence
    lemmatizedData = [x.encode('utf-8') for x in lemmatizedData]
    return lemmatizedData

if __name__ == '__main__':

    print "Data will be augmented by : ", NUMBER_TO_ADD

    print "Parsing the train data....."
    parse_data(TRAIN_FILE, "train.csv")

    #parse test data and save as csv
    print "Parsing the test data....."
    parse_data(TEST_FILE, "test.csv")
    
    print "Reading the train and test data..."
    trainData = pd.read_csv("train.csv", header=0, delimiter="\t", quoting=3)
    files = ["loc.csv", "hum.csv", "desc.csv", "num.csv", "enty.csv"]
    labels_to_add = NUMBER_TO_ADD
    for filename in files:
        x = pd.read_csv(filename, header=None, delimiter="\t", nrows=labels_to_add, names=['id', 'question', 'label'])
        trainData = pd.concat([trainData, x])
        # print "Lines added from ", filename, " is : ", len(x)

    #shuffle rows in the training data so that all lables are spread across the dataframe and not concentrated in a place
    trainData = trainData.iloc[np.random.permutation(len(trainData))]

    testData = pd.read_csv("test.csv", header=0, delimiter="\t", quoting=3)

    print "Lemmatizing the data.."
    #lemmatizing words
    nlp = English()
    trainDataLemma = lemmatize(trainData["question"])
    testDataLemma = lemmatize(testData["question"])

    training = trainData["question"]+" "+trainDataLemma
    testing = testData["question"]+" "+testDataLemma

    #fitting CountVectorizer on training question
    print "Converting data to matrix of token counts using CountVectorizer....."
    vectorizer = CountVectorizer(analyzer="word", tokenizer=None, preprocessor=None, stop_words=None, ngram_range=(1, 2), dtype=np.int8)
    trainDataFeatures = vectorizer.fit_transform(training)
    testDataFeatures = vectorizer.transform(testing)

    #Logistic Regression with C=1 and default param
    print "Performing Logistic Regression"
    logreg = LogisticRegression()
    logreg.fit(trainDataFeatures, trainData["label"])
    resultLogreg = logreg.predict(testDataFeatures)
    outputLogreg = pd.DataFrame(data={"id":testData["id"], "question":testData["question"], "original_label":testData["label"], "output_label":resultLogreg})
    print "Results for Logistic Regression"
    print "F1_score = {}".format(f1_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    print "Precision = {}".format(precision_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    print "Recall = {}".format(recall_score(outputLogreg["original_label"], outputLogreg["output_label"], average="macro"), end='\t')
    print "Accuracy = {}".format(accuracy_score(outputLogreg["original_label"], outputLogreg["output_label"]))

    # confusion matrix to find classwise accuracy
    # cmat = confusion_matrix(outputLogreg["original_label"], outputLogreg["output_label"])
    # print (cmat.diagonal().astype('float32')/cmat.sum(axis=1).astype('float32'))

    #LinearSVC with C=1 and default param
    print "Performing LinearSVC"
    clf = svm.LinearSVC()
    clf.fit(trainDataFeatures, trainData["label"])
    resultLinearSVC = clf.predict(testDataFeatures)
    outputLinearSVC = pd.DataFrame(data={"id":testData["id"], "question":testData["question"], "original_label":testData["label"], "output_label":resultLinearSVC})
    print "Results for LinearSVC"
    print "F1_score = {}".format(f1_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    print "Precision = {}".format(precision_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    print "Recall = {}".format(recall_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"], average="macro"), end='\t')
    print "Accuracy = {}".format(accuracy_score(outputLinearSVC["original_label"], outputLinearSVC["output_label"]))

    # confusion matrix to find classwise accuracy
    # cmat = confusion_matrix(outputLinearSVC["original_label"], outputLinearSVC["output_label"])
    # print (cmat.diagonal().astype('float32')/cmat.sum(axis=1).astype('float32'))
